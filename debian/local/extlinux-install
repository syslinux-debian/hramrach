#!/bin/sh

## Copyright (C) 2006-2013 Daniel Baumann <mail@daniel-baumann.ch>
##
## This program comes with ABSOLUTELY NO WARRANTY; for details see COPYING.
## This is free software, and you are welcome to redistribute it
## under certain conditions; see COPYING for details.


set -e

_DEVICE="${1}"

# Exit if user has not specified a device to install
if [ -z "${_DEVICE}" ]
then
	echo "E: Usage: ${0} DEVICE"
	exit 1
else
	shift
fi

_DIRECTORY="/boot/extlinux"

# Exit if user is unprivileged
if [ "$(id -u)" -ne 0 ]
then
	echo "E: need root privileges"
	exit 1
fi

# Exit if user has specified a non-existing device
if [ ! -e "${_DEVICE}" ]
then
	echo "E: cannot access \"${_DEVICE}\": No such device"
	exit 1
fi

# Exit if user has specified an invalid device
if [ ! -b "${_DEVICE}" ]
then
	echo "E: cannot access \"${_DEVICE}\": No valid device"
	exit 1
fi

# Checking extlinux directory
echo -n "P: Checking for EXTLINUX directory..."

# Creating extlinux directory
if [ ! -e "${_DIRECTORY}" ]
then
	echo " not found."

	echo -n "P: Creating EXTLINUX directory..."
	mkdir -p "${_DIRECTORY}"
	echo " done: ${_DIRECTORY}"
else
	echo " found."
fi

_MBR="$(/sbin/blkid -s PART_ENTRY_SCHEME -p -o value ${_DEVICE})"

case "${_MBR}" in
	gpt)
		_MBR="gptmbr"
		;;

	*)
		_MBR="mbr"
		;;
esac

# Searching syslinux MBR
if [ ! -e /usr/lib/EXTLINUX/${_MBR}.bin ]
then
	echo "E: /usr/lib/EXTLINUX/${_MBR}.bin: No such file"
	exit 1
fi

# Saving old MBR
echo -n "P: Saving old ${_MBR}..."
dd if="${_DEVICE}" of=/boot/${_MBR}-$(basename "${_DEVICE}").old bs=440 count=1 conv=notrunc 2> /dev/null
echo " done: /boot/${_MBR}-$(basename "${_DEVICE}").old"

# Writing syslinux MBR
echo -n "P: Writing new ${_MBR}..."
dd if=/usr/lib/EXTLINUX/${_MBR}.bin of="${_DEVICE}" bs=440 count=1 conv=notrunc 2> /dev/null
echo " done: ${_DEVICE}"

# Writing extlinux loader
echo "P: Installing EXTLINUX..."
extlinux --install "${_DIRECTORY}" ${@}
