
# config_file
config_to_var(){
	echo -n EXTLINUX_ ; echo "$1" | tr "a-z" "A-Z" | tr - _
}

# variable default
get_var(){
	eval echo "\"\${$1:-$2}\""
}

# variable value
set_var(){
	eval "$1=\"$2\""
}
